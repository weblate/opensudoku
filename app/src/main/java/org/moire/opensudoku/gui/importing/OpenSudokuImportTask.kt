/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui.importing

import android.content.Context
import android.net.Uri
import android.text.format.DateFormat
import androidx.core.text.isDigitsOnly
import org.moire.opensudoku.R
import org.moire.opensudoku.db.SudokuInvalidFormatException
import org.moire.opensudoku.db.forEach
import org.moire.opensudoku.db.id
import org.moire.opensudoku.db.originalValues
import org.moire.opensudoku.game.CellCollection
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.gui.Tag
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import java.net.URI
import java.util.Date

/**
 * Handles import of .opensudoku files.
 */
class OpenSudokuImportTask(private val mUri: Uri) : AbstractImportTask() {
	@Throws(SudokuInvalidFormatException::class)
	override suspend fun processImport(context: Context) {
		val streamReader: InputStreamReader = if (mUri.scheme == "content") {
			val contentResolver = context.contentResolver
			InputStreamReader(contentResolver.openInputStream(mUri))
		} else {
			val juri = URI(mUri.scheme, mUri.schemeSpecificPart, mUri.fragment)
			InputStreamReader(juri.toURL().openStream())
		}
		streamReader.use { importXml(context, it) }
	}

	@Throws(SudokuInvalidFormatException::class)
	private suspend fun importXml(context: Context, reader: Reader) {
		val inBR = BufferedReader(reader)
		val parserFactory: XmlPullParserFactory = XmlPullParserFactory.newInstance()
		parserFactory.isNamespaceAware = false
		val parser: XmlPullParser = parserFactory.newPullParser()
		parser.setInput(inBR)
		var eventType = parser.eventType
		var rootTag: String

		while (eventType != XmlPullParser.END_DOCUMENT) {
			if (eventType == XmlPullParser.START_TAG) {
				rootTag = parser.name
				if (rootTag == "opensudoku") {
					when (parser.getAttributeValue(null, "version")) {
						null -> {
							importOpenSudokuV1Puzzles(context, parser)
						}

						"2" -> {
							importOpenSudokuV2Puzzles(parser)
						}

						"3" -> {
							importOpenSudokuV3Puzzles(parser)
						}

						else -> {
							importError = context.getString(R.string.invalid_format)
						}
					}
				} else {
					importError = context.getString(R.string.invalid_format)
					return
				}
			}
			eventType = parser.next()
		}
	}

	@Throws(XmlPullParserException::class, IOException::class, SudokuInvalidFormatException::class)
	private suspend fun importOpenSudokuV3Puzzles(parser: XmlPullParser) {
		var eventType = parser.eventType
		var lastTag: String
		var lastFolderId: Long = 0
		val existingPuzzles = HashMap<String, Long>()

		mDatabase.getPuzzleListCursor().forEach { c -> existingPuzzles[c.originalValues] = c.id }
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if (eventType == XmlPullParser.START_TAG) {
				lastTag = parser.name
				if (lastTag == Tag.FOLDER) {
					val name = parser.getAttributeValue(null, Tag.NAME)
					val created = parser.getAttributeValue(null, Tag.CREATED).toLong()
					lastFolderId = importFolder(name, created)
				} else if (lastTag == Tag.GAME) {
					val (data, dataVersion) = CellCollection.deserialize(parser.getAttributeValue(null, Tag.CELLS_DATA))
					with(SudokuGame(data)) {
						created = parser.getAttributeValue(null, Tag.CREATED).toLong()
						lastPlayed = parser.getAttributeValue(null, Tag.LAST_PLAYED).toLong()
						state = parser.getAttributeValue(null, Tag.STATE).toInt()
						mistakeCounter = parser.getAttributeValue(null, Tag.MISTAKE_COUNTER)?.toInt() ?: 0
						playingDuration = parser.getAttributeValue(null, Tag.TIME).toLong()
						userNote = parser.getAttributeValue(null, Tag.USER_NOTE)
						folderId = lastFolderId
						commandStack.deserialize(parser.getAttributeValue(null, Tag.COMMAND_STACK), dataVersion)
						id = existingPuzzles[mCells.originalValues] ?: -1L

						// update database
						if (id == -1L) {
							val newId = mDatabase.insertPuzzle(this)
							existingPuzzles[mCells.originalValues] = newId
							importedCount += 1
						} else if (state != SudokuGame.GAME_STATE_NOT_STARTED) {
							mDatabase.updatePuzzle(this)    // update makes sense for started puzzles
							updatedCount += 1
						} else {
							duplicatesCount += 1
						}
					}
					mProgressUpdate.maxValue = importedCount + updatedCount + duplicatesCount
				}
			}
			eventType = parser.next()
		}
	}

	@Throws(XmlPullParserException::class, IOException::class, SudokuInvalidFormatException::class)
	private suspend fun importOpenSudokuV2Puzzles(parser: XmlPullParser) {
		var eventType = parser.eventType
		var lastTag: String
		var lastFolderId: Long = 0
		val existingPuzzles = HashMap<String, Long>()

		mDatabase.getPuzzleListCursor().forEach { c -> existingPuzzles[c.originalValues] = c.id }
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if (eventType == XmlPullParser.START_TAG) {
				lastTag = parser.name
				if (lastTag == Tag.FOLDER) {
					val name = parser.getAttributeValue(null, Tag.NAME)
					val created = parser.getAttributeValue(null, Tag.CREATED).toLong()
					lastFolderId = importFolder(name, created)
				} else if (lastTag == Tag.GAME) {
					parser.getAttributeValue(null, "data")?.run {
						val (data, dataVersion) = CellCollection.deserialize(this)
						with(SudokuGame(data)) {
							state = parser.getAttributeValue(null, Tag.STATE)?.toInt() ?: SudokuGame.GAME_STATE_NOT_STARTED
							playingDuration = parser.getAttributeValue(null, Tag.TIME)?.toLong() ?: 0
							created = parser.getAttributeValue(null, Tag.CREATED)?.toLong() ?: 0
							lastPlayed = parser.getAttributeValue(null, Tag.LAST_PLAYED)?.toLong() ?: 0
							userNote = parser.getAttributeValue(null, "note") ?: ""
							commandStack.deserialize(parser.getAttributeValue(null, Tag.COMMAND_STACK), dataVersion)
							folderId = lastFolderId
							id = existingPuzzles[mCells.originalValues] ?: -1L

							// update database
							if (id == -1L) {
								val newId = mDatabase.insertPuzzle(this)
								existingPuzzles[mCells.originalValues] = newId
								importedCount += 1
							} else if (state != SudokuGame.GAME_STATE_NOT_STARTED) {
								mDatabase.updatePuzzle(this)    // update makes sense only for started puzzles import
								updatedCount += 1
							} else {
								duplicatesCount += 1
							}
						}
					}
					mProgressUpdate.maxValue = importedCount + updatedCount + duplicatesCount
				}
			}
			eventType = parser.next()
		}
	}

	@Throws(XmlPullParserException::class, IOException::class, SudokuInvalidFormatException::class)
	private suspend fun importOpenSudokuV1Puzzles(context: Context, parser: XmlPullParser) {
		var eventType = parser.eventType
		var lastTag = ""
		var folderId: Long = -1L

		val newPuzzles = HashSet<String>()
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if (eventType == XmlPullParser.START_TAG) {
				lastTag = parser.name
				if (lastTag == "game") {
					val cellsValues = parser.getAttributeValue(null, "data")
					if (cellsValues.length == 81 && cellsValues.isDigitsOnly()) {
						newPuzzles.add(cellsValues)
						mProgressUpdate.maxValue = newPuzzles.size
					}
				}
			} else if (eventType == XmlPullParser.END_TAG) {
				lastTag = ""
			} else if (eventType == XmlPullParser.TEXT) {
				if (lastTag == "name") {
					folderId = importFolder(parser.text)
				}
			}
			eventType = parser.next()
		}
		if (folderId == -1L) {
			folderId = importFolder(
				"${context.getString(R.string.import_title)} ${DateFormat.format("yyyy-MM-dd HH:mm:ss", Date())}"
			)
		}

		// skip existing puzzles and count duplicates
		mDatabase.getPuzzleListCursor().forEach { c -> if (newPuzzles.remove(c.originalValues)) duplicatesCount += 1 }

		mProgressUpdate.currentValue = 0
		mProgressUpdate.maxValue = newPuzzles.size
		for (values in newPuzzles) {
			mProgressUpdate.currentValue += 1
			mDatabase.insertPuzzle(values, folderId)
			importedCount += 1
		}
	}
}
