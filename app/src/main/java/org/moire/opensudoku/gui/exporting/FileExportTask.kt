/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui.exporting

import android.content.Context
import android.util.Log
import android.util.Xml
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.moire.opensudoku.db.SudokuDatabase
import org.moire.opensudoku.db.extractSudokuGameFromCursorRow
import org.moire.opensudoku.db.forEach
import org.moire.opensudoku.game.FolderInfo
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.gui.ProgressUpdater
import org.moire.opensudoku.gui.PuzzleExportActivity.Companion.ALL_IDS
import org.moire.opensudoku.gui.Tag
import org.xmlpull.v1.XmlSerializer
import java.io.BufferedWriter
import java.io.IOException
import java.io.OutputStreamWriter
import java.io.Writer

class FileExportTask {
	/**
	 * Occurs when export is finished.
	 */
	var onExportFinishedListener: (suspend (FileExportTaskResult?) -> Unit)? = null

	internal suspend fun exportToFile(context: Context, params: FileExportTaskParams, progressUpdater: ProgressUpdater) {
		withContext(Dispatchers.IO) {
			val result = saveToFile(params, context, progressUpdater)
			launch {
				onExportFinishedListener?.invoke(result)
			}
		}
	}

	private fun saveToFile(exportParams: FileExportTaskParams, context: Context, progressUpdater: ProgressUpdater): FileExportTaskResult {
		requireNotNull(exportParams.folderId) { "'folderId' param must be set" }
		requireNotNull(exportParams.fileOutputStream) { "Output stream cannot be null" }
		val result = FileExportTaskResult()
		result.isSuccess = true
		result.filename = exportParams.filename
		var writer: Writer? = null
		try {
			val serializer = Xml.newSerializer()
			writer = BufferedWriter(OutputStreamWriter(exportParams.fileOutputStream))
			serializer.setOutput(writer)
			serializer.startDocument("UTF-8", true)
			serializer.startTag("", "opensudoku")
			serializer.attribute("", "version", FILE_EXPORT_VERSION)

			SudokuDatabase(context, true).use { db ->
				serializeFolders(db, serializer, exportParams.folderId!!, exportParams.puzzleId ?: ALL_IDS, progressUpdater)
			}

			serializer.endTag("", "opensudoku")
			serializer.endDocument()
		} catch (e: IOException) {
			Log.e(javaClass.simpleName, "Error while exporting file.", e)
			result.isSuccess = false
			return result
		} finally {
			if (writer != null) {
				try {
					writer.close()
				} catch (e: IOException) {
					Log.e(javaClass.simpleName, "Error while exporting file.", e)
					result.isSuccess = false
				}
			}
		}
		return result
	}

	private fun serializeFolders(db: SudokuDatabase, serializer: XmlSerializer, folderId: Long, gameId: Long = ALL_IDS, progressUpdater: ProgressUpdater) {
		val folderList: List<FolderInfo> = if (folderId == -1L) db.getFolderList() else listOf(db.getFolderInfo(folderId)!!)
		for (folder in folderList) {
			serializer.startTag("", Tag.FOLDER)
			serializer.attribute("", Tag.NAME, folder.name)
			serializer.attribute("", Tag.CREATED, folder.created.toString())
			progressUpdater.titleParam = folder.name

			var exportedCount = 0
			if (gameId == ALL_IDS) {
				db.getPuzzleListCursor(folder.id).forEach { cursor ->
					serializeGame(serializer, extractSudokuGameFromCursorRow(cursor))
					exportedCount += 1
					progressUpdater.currentValue = exportedCount
					progressUpdater.maxValue = cursor.count
				}
			} else {
				serializeGame(serializer, db.getPuzzle(gameId)!!)
			}

			serializer.endTag("", Tag.FOLDER)
		}
	}

	private fun serializeGame(serializer: XmlSerializer, game: SudokuGame) {
		serializer.startTag("", Tag.GAME)
		serializer.attribute("", Tag.CREATED, game.created.toString())
		serializer.attribute("", Tag.STATE, game.state.toString())
		serializer.attribute("", Tag.MISTAKE_COUNTER, game.mistakeCounter.toString())
		serializer.attribute("", Tag.TIME, game.playingDuration.toString())
		serializer.attribute("", Tag.LAST_PLAYED, game.lastPlayed.toString())
		serializer.attribute("", Tag.CELLS_DATA, game.mCells.serialize())
		serializer.attribute("", Tag.USER_NOTE, game.userNote)
		serializer.attribute("", Tag.COMMAND_STACK, game.commandStack.serialize())
		serializer.endTag("", Tag.GAME)
	}

	companion object {
		val FILE_EXPORT_VERSION: String get() = "3"
	}
}
