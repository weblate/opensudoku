/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.gui.inputmethod

import android.app.Dialog
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.button.MaterialButton
import org.moire.opensudoku.R
import org.moire.opensudoku.gui.NumberButton
import org.moire.opensudoku.gui.SudokuBoardView

/**
 * Dialog for selecting and entering values and marks.
 *
 * When entering a value, the dialog automatically closes.
 *
 * When entering a mark the dialog remains open, to allow multiple marks to be entered at once.
 */
class IMPopupDialog(val parent: ViewGroup, mContext: Context, mBoard: SudokuBoardView) : Dialog(mContext) {
	private val mInflater: LayoutInflater
	private var mEditMode = InputMethod.MODE_EDIT_VALUE
	val mNumberButtons: MutableMap<Int, NumberButton> = HashMap()

	// selected number on "Select number" tab (0 if nothing is selected).
	private var mSelectedNumber = 0
	private val mCentralMarksSelectedNumbers: MutableSet<Int> = HashSet()
	private val mCornerMarksSelectedNumbers: MutableSet<Int> = HashSet()
	private var mShowNumberTotals = false

	/** True if buttons with completed values should be highlighted  */
	private var mHighlightCompletedValues = false
	private val mEnterNumberButton: MaterialButton
	private val mCentralMarksButton: MaterialButton
	val mCornerMarksButton: MaterialButton

	/**
	 * Callback to be invoked when user selects a new cell value and/or central/corner marks.
	 */
	internal lateinit var cellUpdateCallback: ((value: Int, centralMarks: Array<Int>, cornerMarks: Array<Int>) -> Unit)

	private val mValueCount: MutableMap<Int, Int> = HashMap()

	private val mNumberButtonClicked = View.OnClickListener { v: View ->
		val number = v.tag as Int
		when (mEditMode) {
			InputMethod.MODE_EDIT_VALUE -> {
				mSelectedNumber = number
				syncAndDismiss()
			}

			InputMethod.MODE_EDIT_CENTRAL_MARKS -> if ((v as MaterialButton).isChecked) {
				mCentralMarksSelectedNumbers.add(number)
			} else {
				mCentralMarksSelectedNumbers.remove(number)
			}

			InputMethod.MODE_EDIT_CORNER_MARKS -> if ((v as MaterialButton).isChecked) {
				mCornerMarksSelectedNumbers.add(number)
			} else {
				mCornerMarksSelectedNumbers.remove(number)
			}
		}
	}

	/**
	 * Occurs when user presses "Clear" button.
	 */
	private val clearButtonListener = View.OnClickListener { v ->
		(v as MaterialButton).isChecked = false
		when (mEditMode) {
			InputMethod.MODE_EDIT_VALUE -> {
				mSelectedNumber = 0
				syncAndDismiss()
			}

			InputMethod.MODE_EDIT_CENTRAL_MARKS ->                     // Clear the central marks. Dialog should stay visible
				setCentralMarks(emptyList())

			InputMethod.MODE_EDIT_CORNER_MARKS ->                     // Clear the corner marks. Dialog should stay visible
				setCornerMarks(emptyList())
		}
		update()
	}

	/**
	 * Occurs when user presses "Close" button.
	 */
	private val closeButtonListener = View.OnClickListener { _: View? -> syncAndDismiss() }

	/** Synchronises state with the hosting activity and dismisses the dialog  */
	private fun syncAndDismiss() {
		cellUpdateCallback(mSelectedNumber, mCentralMarksSelectedNumbers.toTypedArray(), mCornerMarksSelectedNumbers.toTypedArray())
		dismiss()
	}

	init {
		mInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
		val keypad = mInflater.inflate(R.layout.im_popup_edit_value, parent, false)

		mNumberButtons[1] = keypad.findViewById(R.id.button_1)
		mNumberButtons[2] = keypad.findViewById(R.id.button_2)
		mNumberButtons[3] = keypad.findViewById(R.id.button_3)
		mNumberButtons[4] = keypad.findViewById(R.id.button_4)
		mNumberButtons[5] = keypad.findViewById(R.id.button_5)
		mNumberButtons[6] = keypad.findViewById(R.id.button_6)
		mNumberButtons[7] = keypad.findViewById(R.id.button_7)
		mNumberButtons[8] = keypad.findViewById(R.id.button_8)
		mNumberButtons[9] = keypad.findViewById(R.id.button_9)

		val colorText: ColorStateList = InputMethod.makeTextColorStateList(mBoard)
		val colorBackground: ColorStateList = InputMethod.makeBackgroundColorStateList(mBoard)

		for ((key, b) in mNumberButtons) {
			b.tag = key
			b.setOnClickListener(mNumberButtonClicked)
			b.enableAllNumbersPlaced = mHighlightCompletedValues
			b.backgroundTintList = colorBackground
			b.setTextColor(colorText)
		}

		val clearButton = keypad.findViewById<MaterialButton>(R.id.button_clear)
		clearButton.tag = 0
		clearButton.setOnClickListener(clearButtonListener)
		clearButton.backgroundTintList = colorBackground
		clearButton.iconTint = colorText

		/* Switch mode, and update the UI */
		val modeButtonClicked = View.OnClickListener { v: View ->
			mEditMode = v.tag as Int
			update()
		}

		mEnterNumberButton = with(keypad.findViewById<MaterialButton>(R.id.enter_number)) {
			tag = InputMethod.MODE_EDIT_VALUE
			setOnClickListener(modeButtonClicked)
			backgroundTintList = colorBackground
			iconTint = colorText
			this
		}

		mCentralMarksButton = with(keypad.findViewById<MaterialButton>(R.id.central_mark)) {
			tag = InputMethod.MODE_EDIT_CENTRAL_MARKS
			setOnClickListener(modeButtonClicked)
			backgroundTintList = colorBackground
			iconTint = colorText
			this
		}

		mCornerMarksButton = with(keypad.findViewById<MaterialButton>(R.id.corner_mark)) {
			tag = InputMethod.MODE_EDIT_CORNER_MARKS
			setOnClickListener(modeButtonClicked)
			backgroundTintList = colorBackground
			iconTint = colorText
			this
		}

		val closeButton = keypad.findViewById<View>(R.id.button_close)
		closeButton.setOnClickListener(closeButtonListener)
		setContentView(keypad)
	}

	private fun update() {
		// Determine which buttons to check, based on the value / marks in the selected cell
		val buttonsToCheck: List<Int>
		when (mEditMode) {
			InputMethod.MODE_EDIT_VALUE -> {
				mEnterNumberButton.isChecked = true
				mCentralMarksButton.isChecked = false
				mCornerMarksButton.isChecked = false
				buttonsToCheck = listOf(mSelectedNumber)
			}

			InputMethod.MODE_EDIT_CENTRAL_MARKS -> {
				mEnterNumberButton.isChecked = false
				mCentralMarksButton.isChecked = true
				mCornerMarksButton.isChecked = false
				buttonsToCheck = ArrayList(mCentralMarksSelectedNumbers)
			}

			InputMethod.MODE_EDIT_CORNER_MARKS -> {
				mEnterNumberButton.isChecked = false
				mCentralMarksButton.isChecked = false
				mCornerMarksButton.isChecked = true
				buttonsToCheck = ArrayList(mCornerMarksSelectedNumbers)
			}

			else ->                 // Can't happen
				buttonsToCheck = ArrayList()
		}
		for (button in mNumberButtons.values) {
			val tag = button.tag as Int
			button.mode = mEditMode

			// Check the button if necessary
			button.isChecked = buttonsToCheck.contains(tag)

			// Update the count of numbers placed
			if (mValueCount.isNotEmpty()) {
				button.setNumbersPlaced(mValueCount[tag] ?: 0)
			}
		}
	}

	fun setShowNumberTotals(showNumberTotals: Boolean) {
		if (mShowNumberTotals == showNumberTotals) {
			return
		}
		mShowNumberTotals = showNumberTotals
		for (button in mNumberButtons.values) {
			button.showNumbersPlaced = mShowNumberTotals
		}
	}

	fun setHighlightCompletedValues(highlightCompletedValues: Boolean) {
		if (mHighlightCompletedValues == highlightCompletedValues) {
			return
		}
		mHighlightCompletedValues = highlightCompletedValues
		for (b in mNumberButtons.values) {
			b.enableAllNumbersPlaced = mHighlightCompletedValues
		}
	}

	/**
	 * Reset most of the state of the dialog (selected values, marks, etc).
	 *
	 * DO NOT reset the edit mode, for compatibility with the previous code that used a tab.
	 * The selected tab (which was the edit mode) was retained if the dialog was dismissed on
	 * one cell and opened on another.
	 */
	fun resetState() {
		mSelectedNumber = 0
		mCentralMarksSelectedNumbers.clear()
		mCornerMarksSelectedNumbers.clear()
		mValueCount.clear()
		update()
	}

	fun setNumber(number: Int) {
		mSelectedNumber = number
		update()
	}

	fun setCentralMarks(numbers: List<Int>) {
		mCentralMarksSelectedNumbers.clear()
		mCentralMarksSelectedNumbers.addAll(numbers)
		update()
	}

	fun setCornerMarks(numbers: List<Int>) {
		mCornerMarksSelectedNumbers.clear()
		mCornerMarksSelectedNumbers.addAll(numbers)
		update()
	}

	fun setValueCount(count: Map<Int, Int>) {
		mValueCount.clear()
		mValueCount.putAll(count)
		update()
	}
}
