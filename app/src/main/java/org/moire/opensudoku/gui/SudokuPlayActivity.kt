/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2024 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.gui

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.preference.PreferenceManager
import org.moire.opensudoku.R
import org.moire.opensudoku.db.SudokuDatabase
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.gui.SudokuBoardView.HighlightMode
import org.moire.opensudoku.gui.Tag.PUZZLE_ID
import org.moire.opensudoku.gui.fragments.SimpleDialog
import org.moire.opensudoku.gui.inputmethod.IMControlPanel
import org.moire.opensudoku.gui.inputmethod.IMControlPanelStatePersister
import org.moire.opensudoku.gui.inputmethod.IMInsertOnTap
import org.moire.opensudoku.gui.inputmethod.IMPopup
import org.moire.opensudoku.gui.inputmethod.IMSelectOnTap
import org.moire.opensudoku.utils.ThemeUtils

private const val double_marks_enabled = "double_marks_enabled"
private const val highlight_directly_wrong_values = "highlight_directly_wrong_values"
private const val highlight_indirectly_wrong_values = "highlight_indirectly_wrong_values"
private const val highlight_touched_cell = "highlight_touched_cell"
private const val highlight_completed_values = "highlight_completed_values"
private const val show_number_totals = "show_number_totals"

class SudokuPlayActivity : ThemedActivity() {
	private var bellEnabled: Boolean = true
	private lateinit var settingsLauncher: ActivityResultLauncher<Intent>
	private lateinit var mSudokuGame: SudokuGame
	private lateinit var mDatabase: SudokuDatabase
	private lateinit var mRootLayout: ViewGroup
	private lateinit var mSudokuBoard: SudokuBoardView
	private lateinit var mOptionsMenu: Menu
	private lateinit var mIMControlPanel: IMControlPanel
	private lateinit var mIMControlPanelStatePersister: IMControlPanelStatePersister
	private lateinit var mIMPopup: IMPopup
	private lateinit var mIMInsertOnTap: IMInsertOnTap
	private lateinit var mIMSelectOnTap: IMSelectOnTap
	private var mShowTime = true
	private lateinit var mGameTimer: GameTimer
	private val mPlayingDurationFormatter = PlayingDurationFormat()
	private var mFillInMarksEnabled = false
	private lateinit var mHintsQueue: HintsQueue

	/**
	 * Occurs when puzzle is solved.
	 */
	private val onSolvedListener = {
		if (mShowTime) {
			mGameTimer.stop()
		}
		mSudokuBoard.isReadOnly = (true)
		if (mSudokuGame.usedSolver()) {
			SimpleDialog(supportFragmentManager).show(R.string.used_solver)

		} else {
			with(SimpleDialog(supportFragmentManager)) {
				iconId = R.drawable.ic_info
				titleId = R.string.well_done
				message = this@SudokuPlayActivity.getString(
					R.string.congrats,
					mPlayingDurationFormatter.format(mSudokuGame.playingDuration),
					mSudokuGame.mistakeCounter
				)
				positiveButtonCallback = ::finish
				show()
			}
		}
	}

	/**
	 * Occurs when valid 9 entries of a digit are placed.
	 */
	private val onDigitFinishedListener: ((Int) -> Unit) = { digit ->
		mSudokuBoard.blinkValue(digit)
		playTone(ToneGenerator.TONE_PROP_ACK)
	}

	private val onInvalidDigitEntryListener: (() -> Unit) = {
		playTone(ToneGenerator.TONE_SUP_ERROR)
	}

	private fun playTone(toneType: Int) {
		val audioService = getSystemService(Context.AUDIO_SERVICE) as AudioManager
		if (bellEnabled && !audioService.isStreamMute(AudioManager.STREAM_MUSIC) && audioService.ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			val volume = 100 * audioService.getStreamVolume(AudioManager.STREAM_MUSIC) / audioService.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
			ToneGenerator(AudioManager.STREAM_MUSIC, volume).startTone(toneType, 300)
		}
	}

	private val onSelectedNumberChangedListener: (Int) -> Unit = {
		mSudokuBoard.highlightedValue = it
		mSudokuBoard.clearCellSelection()
	}

	public override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.sudoku_play)
		mRootLayout = findViewById(R.id.play_root_layout)
		mSudokuBoard = findViewById(R.id.play_board_view)
		mDatabase = SudokuDatabase(applicationContext, false)
		mHintsQueue = HintsQueue(this)
		mGameTimer = GameTimer(this)

		// create sudoku game instance
		if (savedInstanceState == null) {
			// activity runs for the first time, read game from database
			val mSudokuGameID = intent.getLongExtra(PUZZLE_ID, 0)
			mSudokuGame = mDatabase.getPuzzle(mSudokuGameID) ?: SudokuGame.createEmptyGame()
		} else {
			// activity has been running before, restore its state
			mSudokuGame = SudokuGame.createEmptyGame()
			mSudokuGame.restoreState(savedInstanceState)
			mGameTimer.restoreState(savedInstanceState)
		}

		// save our most recently played puzzle
		val gameSettings = PreferenceManager.getDefaultSharedPreferences(applicationContext)
		val editor = gameSettings.edit()
		editor.putLong("most_recently_played_puzzle_id", mSudokuGame.id)
		editor.apply()
		if (mSudokuGame.state == SudokuGame.GAME_STATE_NOT_STARTED) {
			mSudokuGame.start()
		} else if (mSudokuGame.state == SudokuGame.GAME_STATE_PLAYING) {
			mSudokuGame.resume()
		}
		if (mSudokuGame.state == SudokuGame.GAME_STATE_COMPLETED) {
			mSudokuBoard.isReadOnly = true
		}
		mSudokuBoard.setGame(mSudokuGame)
		mSudokuBoard.onReadonlyChangeListener = ::updateUndoButtonState
		mSudokuGame.onHasUndoChangedListener = ::updateUndoButtonState
		mSudokuGame.onPuzzleSolvedListener = onSolvedListener
		mSudokuGame.onDigitFinishedManuallyListener = onDigitFinishedListener
		mSudokuGame.onInvalidDigitEnteredListener = onInvalidDigitEntryListener
		mHintsQueue.showOneTimeHint("welcome", R.string.welcome, R.string.first_run_hint)
		mIMControlPanel = findViewById(R.id.input_methods)
		mIMControlPanel.initialize(mSudokuBoard, mSudokuGame, mHintsQueue)
		mIMControlPanelStatePersister = IMControlPanelStatePersister(this)
		mIMPopup = mIMControlPanel.imPopup
		mIMInsertOnTap = mIMControlPanel.imInsertOnTap
		mIMSelectOnTap = mIMControlPanel.imSelectOnTap
		if (!mSudokuBoard.isReadOnly) selectCell(mSudokuGame.lastCommandCell)

		settingsLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { restartActivity() }
	}

	private fun updateUndoButtonState() {
		mOptionsMenu.findItem(MenuItems.UNDO_ACTION.id)?.isEnabled = !mSudokuBoard.isReadOnly && mSudokuGame.hasSomethingToUndo()
	}

	override fun onResume() {
		super.onResume()

		// read game settings
		val gameSettings = PreferenceManager.getDefaultSharedPreferences(applicationContext)
		val screenPadding = gameSettings.getInt("screen_border_size", 0)
		mRootLayout.setPadding(screenPadding, screenPadding, screenPadding, screenPadding)
		mFillInMarksEnabled = gameSettings.getBoolean("fill_in_marks_enabled", false)
		bellEnabled = gameSettings.getBoolean("bell_enabled", true)
		val theme = gameSettings.getString("theme", "opensudoku")
		if (theme == "custom" || theme == "custom_light") {
			ThemeUtils.applyCustomThemeToSudokuBoardViewFromSharedPreferences(this, mSudokuBoard)
		} else {
			mSudokuBoard.setAllColorsFromThemedContext(this)
		}
		mSudokuBoard.highlightDirectlyWrongValues = gameSettings.getBoolean(highlight_directly_wrong_values, true)
		val checkIndirectErrors = gameSettings.getBoolean(highlight_indirectly_wrong_values, true)
		mSudokuBoard.highlightIndirectlyWrongValues = checkIndirectErrors
		mSudokuBoard.isDoubleMarksEnabled = gameSettings.getBoolean(double_marks_enabled, true)
		mSudokuBoard.highlightTouchedCell = gameSettings.getBoolean(highlight_touched_cell, true)
		val highlightSimilarCells = gameSettings.getBoolean("highlight_similar_cells", true)
		val highlightSimilarMarks = gameSettings.getBoolean("highlight_similar_marks", true)
		if (highlightSimilarCells) {
			mSudokuBoard.highlightSimilarCells = if (highlightSimilarMarks) HighlightMode.NUMBERS_AND_MARKS else HighlightMode.NUMBERS
		} else {
			mSudokuBoard.highlightSimilarCells = HighlightMode.NONE
		}
		mSudokuGame.removeMarksOnEntry = gameSettings.getBoolean("remove_marks_on_input", true)
		mSudokuGame.checkIndirectErrors = checkIndirectErrors
		mShowTime = gameSettings.getBoolean("show_time", true)
		if (mSudokuGame.state == SudokuGame.GAME_STATE_PLAYING) {
			mSudokuGame.resume()
			if (mShowTime) mGameTimer.start()
		}
		val moveCellSelectionOnPress = gameSettings.getBoolean("im_move_right_on_insert_move_right", false)
		mSudokuBoard.moveCellSelectionOnPress = moveCellSelectionOnPress

		mIMPopup.isEnabled = (gameSettings.getBoolean("im_popup", true))
		mIMPopup.highlightCompletedValues = gameSettings.getBoolean(highlight_completed_values, true)
		mIMPopup.showDigitCount = gameSettings.getBoolean(show_number_totals, false)

		mIMInsertOnTap.isEnabled = (gameSettings.getBoolean("insert_on_tap", true))
		mIMInsertOnTap.highlightCompletedValues = gameSettings.getBoolean(highlight_completed_values, true)
		mIMInsertOnTap.showDigitCount = gameSettings.getBoolean(show_number_totals, false)
		mIMInsertOnTap.bidirectionalSelection = gameSettings.getBoolean("bidirectional_selection", true)
		mIMInsertOnTap.highlightSimilar = gameSettings.getBoolean("highlight_similar", true)
		mIMInsertOnTap.onSelectedNumberChangedListener = onSelectedNumberChangedListener

		mIMSelectOnTap.isEnabled = (gameSettings.getBoolean("select_on_tap", true))
		mIMSelectOnTap.highlightCompletedValues = gameSettings.getBoolean(highlight_completed_values, true)
		mIMSelectOnTap.showDigitCount = gameSettings.getBoolean(show_number_totals, false)
		mIMSelectOnTap.isMoveCellSelectionOnPress = (moveCellSelectionOnPress)

		mIMControlPanel.isDoubleMarksEnabled = gameSettings.getBoolean(double_marks_enabled, true)
		mIMControlPanel.activateFirstInputMethod() // make sure that some input method is activated
		mIMControlPanelStatePersister.restoreState(mIMControlPanel)
		if (!mSudokuBoard.isReadOnly) {
			mSudokuBoard.invokeOnCellSelected()
		}
		updateTime()
	}

	override fun onPause() {
		super.onPause()

		// we will save game to the database as we might not be able to get back
		mDatabase.updatePuzzle(mSudokuGame)
		mGameTimer.stop()
		mIMControlPanel.pause()
		mIMControlPanelStatePersister.saveState(mIMControlPanel)
	}

	override fun onDestroy() {
		super.onDestroy()
		mDatabase.close()
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		mGameTimer.stop()
		if (mSudokuGame.state == SudokuGame.GAME_STATE_PLAYING) {
			mSudokuGame.pause()
		}
		mSudokuGame.saveState(outState)
		mGameTimer.saveState(outState)
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		super.onCreateOptionsMenu(menu)
		mOptionsMenu = menu

		// action menu
		menu.add(0, MenuItems.BELL_ACTION.id, 0, "bell")
			.setIcon(if (bellEnabled) R.drawable.ic_sound_on else R.drawable.ic_sound_off)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
		menu.add(0, MenuItems.UNDO_ACTION.id, 1, R.string.undo)
			.setIcon(R.drawable.ic_undo)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
		updateUndoButtonState()
		menu.add(0, MenuItems.SETTINGS_ACTION.id, 2, R.string.settings)
			.setIcon(R.drawable.ic_settings)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)

		// drop-down menu
		if (mFillInMarksEnabled) {
			menu.add(0, MenuItems.FILL_IN_MAIN_MARKS.id, 1, R.string.fill_in_marks)
				.setIcon(R.drawable.ic_edit)
		}
		menu.add(0, MenuItems.FILL_IN_MAIN_MARKS_WITH_ALL_VALUES.id, 1, R.string.fill_all_marks)
			.setIcon(R.drawable.ic_edit)
		menu.add(0, MenuItems.CLEAR_ALL_MARKS.id, 2, R.string.clear_all_marks)
			.setShortcut('3', 'a')
			.setIcon(R.drawable.ic_delete)
		menu.add(0, MenuItems.SET_CHECKPOINT.id, 3, R.string.set_checkpoint)
		menu.add(0, MenuItems.UNDO_TO_CHECKPOINT.id, 4, R.string.undo_to_checkpoint)
		menu.add(0, MenuItems.UNDO_TO_BEFORE_MISTAKE.id, 4, R.string.undo_to_before_mistake)
		menu.add(0, MenuItems.HINT.id, 5, R.string.hint)
		menu.add(0, MenuItems.SOLVE.id, 6, R.string.solve_puzzle)
		menu.add(0, MenuItems.RESTART.id, 7, R.string.restart)
			.setShortcut('7', 'r')
			.setIcon(R.drawable.ic_restore)
		menu.add(0, MenuItems.SETTINGS.id, 8, R.string.settings)
			.setShortcut('9', 's')
			.setIcon(R.drawable.ic_settings)
		menu.add(0, MenuItems.HELP.id, 9, R.string.help)
			.setShortcut('0', 'h')
			.setIcon(R.drawable.ic_help)


		// Generate any additional actions that can be performed on the
		// overall list.  In a normal install, there are no additional
		// actions found here, but this allows other applications to extend
		// our menu with their own actions.
		val intent = Intent(null, intent.data)
		intent.addCategory(Intent.CATEGORY_ALTERNATIVE)
		menu.addIntentOptions(
			Menu.CATEGORY_ALTERNATIVE, 0, 0, ComponentName(this, SudokuPlayActivity::class.java), null, intent, 0, null
		)
		return true
	}

	override fun onPrepareOptionsMenu(menu: Menu): Boolean {
		super.onPrepareOptionsMenu(menu)
		val isPlaying = (mSudokuGame.state == SudokuGame.GAME_STATE_PLAYING)
		menu.findItem(MenuItems.FILL_IN_MAIN_MARKS_WITH_ALL_VALUES.id)?.isEnabled = isPlaying
		menu.findItem(MenuItems.CLEAR_ALL_MARKS.id)?.isEnabled = isPlaying
		menu.findItem(MenuItems.FILL_IN_MAIN_MARKS.id)?.isEnabled = isPlaying && mFillInMarksEnabled
		menu.findItem(MenuItems.SET_CHECKPOINT.id)?.isEnabled = isPlaying
		menu.findItem(MenuItems.UNDO_TO_CHECKPOINT.id)?.isEnabled = isPlaying && mSudokuGame.hasUndoCheckpoint()
		menu.findItem(MenuItems.UNDO_TO_BEFORE_MISTAKE.id)?.isEnabled = isPlaying && mSudokuGame.mCells.hasMistakes
		menu.findItem(MenuItems.SOLVE.id)?.isEnabled = isPlaying
		menu.findItem(MenuItems.HINT.id)?.isEnabled = isPlaying
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			MenuItems.RESTART.id -> {
				with(SimpleDialog(supportFragmentManager)) {
					iconId = R.drawable.ic_restore
					messageId = R.string.restart_confirm
					positiveButtonCallback = ::restartGame
					show()
				}
				return true
			}

			MenuItems.CLEAR_ALL_MARKS.id -> {
				with(SimpleDialog(supportFragmentManager)) {
					iconId = R.drawable.ic_delete
					messageId = R.string.clear_all_marks_confirm
					positiveButtonCallback = mSudokuGame::clearAllMarksManual
					show()
				}
				return true
			}

			MenuItems.FILL_IN_MAIN_MARKS.id -> {
				mSudokuGame.fillInMarksManual()
				return true
			}

			MenuItems.FILL_IN_MAIN_MARKS_WITH_ALL_VALUES.id -> {
				mSudokuGame.fillInMarksWithAllValuesManual()
				return true
			}

			MenuItems.UNDO_ACTION.id -> {
				val undoneCell = mSudokuGame.undo()
				selectCell(undoneCell)
				return true
			}

			MenuItems.BELL_ACTION.id -> {
				bellEnabled = !bellEnabled
				val gameSettings = PreferenceManager.getDefaultSharedPreferences(applicationContext)
				val editor = gameSettings.edit()
				editor.putBoolean("bell_enabled", bellEnabled)
				editor.apply()
				item.setIcon(if (bellEnabled) R.drawable.ic_sound_on else R.drawable.ic_sound_off)
				return true
			}

			MenuItems.SETTINGS_ACTION.id, MenuItems.SETTINGS.id -> {
				settingsLauncher.launch(Intent(this, GameSettingsActivity::class.java))
				return true
			}

			MenuItems.HELP.id -> {
				mHintsQueue.showHint(R.string.help, R.string.help_text)
				return true
			}

			MenuItems.SET_CHECKPOINT.id -> {
				mSudokuGame.setUndoCheckpoint()
				return true
			}

			MenuItems.UNDO_TO_CHECKPOINT.id -> {
				with(SimpleDialog(supportFragmentManager)) {
					iconId = R.drawable.ic_undo
					messageId = R.string.undo_to_checkpoint_confirm
					positiveButtonCallback = {
						mSudokuGame.undoToCheckpoint()
						selectLastCommandCell()
					}
					show()
				}
				return true
			}

			MenuItems.UNDO_TO_BEFORE_MISTAKE.id -> {
				if (mSudokuGame.solutionCount > 1) {
					SimpleDialog(supportFragmentManager).show(R.string.puzzle_has_multiple_solutions)
				} else if (mSudokuGame.solutionCount < 1) {
					SimpleDialog(supportFragmentManager).show(R.string.puzzle_has_no_solution)
				} else {
					with(SimpleDialog(supportFragmentManager)) {
						iconId = R.drawable.ic_undo
						messageId = R.string.undo_to_before_mistake_confirm
						positiveButtonCallback = {
							mSudokuGame.undoToBeforeMistake()
							selectLastCommandCell()
						}
						show()
					}
				}
				return true
			}

			MenuItems.SOLVE.id -> {
				with(SimpleDialog(supportFragmentManager)) {
					messageId = R.string.solve_puzzle_confirm
					positiveButtonCallback = {
						val numberOfSolutions = mSudokuGame.solve()
						if (numberOfSolutions == 0) {
							SimpleDialog(supportFragmentManager).show(R.string.puzzle_has_no_solution)
						} else if (numberOfSolutions > 1) {
							SimpleDialog(supportFragmentManager).show(R.string.puzzle_has_multiple_solutions)
						}
					}
					show()
				}
				return true
			}

			MenuItems.HINT.id -> {
				with(SimpleDialog(supportFragmentManager)) {
					messageId = R.string.hint_confirm
					positiveButtonCallback = {
						val cell = mSudokuBoard.mSelectedCell
						if (cell != null && cell.isEditable) {
							if (mSudokuGame.solutionCount > 1) {
								SimpleDialog(supportFragmentManager).show(R.string.puzzle_has_multiple_solutions)
							} else if (mSudokuGame.solutionCount < 1) {
								SimpleDialog(supportFragmentManager).show(R.string.puzzle_has_no_solution)
							} else {
								mSudokuGame.solveCell(cell)
							}
						} else {
							SimpleDialog(supportFragmentManager).show(R.string.cannot_give_hint)
						}
					}
					show()
				}
				return true
			}
		}
		return super.onOptionsItemSelected(item)
	}

	private fun restartGame() {
		// Restart game
		mSudokuGame.reset()
		mSudokuGame.start()
		mSudokuBoard.isReadOnly = false
		if (mShowTime) {
			mGameTimer.start()
		}
		mOptionsMenu.findItem(MenuItems.SOLVE.id)?.isEnabled = true
		mOptionsMenu.findItem(MenuItems.HINT.id)?.isEnabled = true
		updateUndoButtonState()
	}

	/**
	 * Restarts whole activity.
	 */
	private fun restartActivity() {
		startActivity(intent)
		finish()
	}

	private fun selectLastCommandCell() {
		mSudokuGame.lastCommandCell.let(::selectCell)
	}

	private fun selectCell(cell: Cell?) {
		if (cell != null) {
			mSudokuBoard.moveCellSelectionTo(cell.rowIndex, cell.columnIndex)
		}
	}

	/**
	 * Update the time of game-play.
	 */
	fun updateTime() {
		if (mShowTime) {
			title = mPlayingDurationFormatter.format(mSudokuGame.playingDuration)
		} else {
			setTitle(R.string.app_name)
		}
	}

	companion object {
		enum class MenuItems {
			RESTART,
			CLEAR_ALL_MARKS,
			FILL_IN_MAIN_MARKS,
			FILL_IN_MAIN_MARKS_WITH_ALL_VALUES,
			UNDO_ACTION,
			HELP,
			SETTINGS_ACTION,
			SETTINGS,
			SET_CHECKPOINT,
			UNDO_TO_CHECKPOINT,
			UNDO_TO_BEFORE_MISTAKE,
			SOLVE,
			HINT,
			BELL_ACTION;

			val id = ordinal + Menu.FIRST
		}

		// This class implements the game clock.  All it does is update the status each tick.
		private class GameTimer(private val sudokuPlayActivity: SudokuPlayActivity) : Timer(1000, Looper.getMainLooper()) {
			override fun step(count: Int, time: Long): Boolean {
				sudokuPlayActivity.updateTime()
				return false // Run until explicitly stopped.
			}
		}
	}
}
