/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.moire.opensudoku.R
import org.moire.opensudoku.db.SudokuDatabase
import org.moire.opensudoku.gui.exporting.FileExportTask
import org.moire.opensudoku.gui.exporting.FileExportTaskParams
import org.moire.opensudoku.gui.fragments.SimpleDialog
import org.moire.opensudoku.utils.getFileName
import java.io.FileNotFoundException
import java.util.Date

class PuzzleExportActivity : ThemedActivity() {
	private lateinit var mFileExportTask: FileExportTask
	private lateinit var mExportParams: FileExportTaskParams

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.progress)
		setTitle(R.string.export)

		mFileExportTask = FileExportTask()
		mExportParams = FileExportTaskParams()
		if (!intent.hasExtra(Tag.FOLDER_ID)) {
			Log.d(javaClass.simpleName, "No 'FOLDER_ID' extra provided, exiting.")
			finish()
			return
		}
		val folderId = intent.getLongExtra(Tag.FOLDER_ID, ALL_IDS)
		mExportParams.folderId = folderId
		val puzzleId = intent.getLongExtra(Tag.PUZZLE_ID, ALL_IDS)
		mExportParams.puzzleId = puzzleId
		val timestamp = DateFormat.format("yyyy-MM-dd-HH-mm-ss", Date()).toString()

		val fileName = if (folderId == ALL_IDS) {
			"all-folders-$timestamp"
		} else {
			val folderName = SudokuDatabase(applicationContext, true).use { database ->
				val folder = database.getFolderInfo(folderId)
				if (folder == null) {
					Log.e(javaClass.simpleName, "Folder with id $folderId not found, exiting.")
					finish()
					return@onCreate
				}
				folder.name
			}
			if (puzzleId != ALL_IDS) "$folderName-$puzzleId-$timestamp" else "$folderName-$timestamp"
		}

		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
			if (result.resultCode == Activity.RESULT_OK) {
				result.data?.data?.let(::startExportToFileTask)
			} else if (result.resultCode == RESULT_CANCELED) {
				finish()
			}
		}.launch(
			Intent(Intent.ACTION_CREATE_DOCUMENT)
				.addCategory(Intent.CATEGORY_OPENABLE)
				.setType("application/x-opensudoku")
				.putExtra(Intent.EXTRA_TITLE, "$fileName.opensudoku")
		)
	}

	private fun startExportToFileTask(uri: Uri) {
		val finishDialog = SimpleDialog(supportFragmentManager)
		finishDialog.onDismiss = ::finish
		mFileExportTask.onExportFinishedListener = { result ->
			withContext(Dispatchers.Main) {
				if (result?.isSuccess == true) {
					finishDialog.show(getString(R.string.puzzles_have_been_exported, result.filename))
				} else {
					finishDialog.show(getString(R.string.unknown_export_error))
				}
			}
		}
		try {
			mExportParams.fileOutputStream = contentResolver.openOutputStream(uri)
			mExportParams.filename = uri.getFileName(contentResolver)
		} catch (e: FileNotFoundException) {
			finishDialog.show(R.string.unknown_export_error)
		}


		with(ProgressUpdater(applicationContext, findViewById(R.id.progress_bar))) {
			titleTextView = findViewById(R.id.progress_introduction)
			titleStringRes = R.string.exporting
			progressTextView = findViewById(R.id.progress_status_text)
			progressStringRes = R.string.exported_num
			this
		}.use { progressUpdater ->
			CoroutineScope(Dispatchers.IO).launch {
				mFileExportTask.exportToFile(this@PuzzleExportActivity, mExportParams, progressUpdater)
			}
		}
	}

	companion object {
		const val ALL_IDS: Long = -1 // export all folders
	}
}

