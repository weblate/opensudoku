/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.moire.opensudoku.R
import org.moire.opensudoku.gui.importing.AbstractImportTask
import org.moire.opensudoku.gui.importing.OpenSudokuImportTask
import org.moire.opensudoku.gui.importing.SdmImportTask
import org.moire.opensudoku.gui.importing.SepbImportTask
import org.moire.opensudoku.gui.importing.SepbRegex
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStreamReader
import java.net.URI
import java.net.URISyntaxException

/**
 * This activity is responsible for importing puzzles from various sources
 * (web, file, .opensudoku, .sdm, extras).
 */
class PuzzleImportActivity : ThemedActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.progress)
		setTitle(R.string.import_title)

		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
			if (result.resultCode == Activity.RESULT_OK) {
				result.data?.data?.let(::startImportFromFileTask)
			} else {
				Log.e(javaClass.simpleName, "No data provided, exiting.")
				finish()
			}
		}.launch(Intent(Intent.ACTION_OPEN_DOCUMENT).addCategory(Intent.CATEGORY_OPENABLE).setType("*/*"))
	}

	private fun startImportFromFileTask(dataUri: Uri) {
		Log.v(javaClass.simpleName, "$dataUri")
		var streamReader: InputStreamReader? = null
		if (dataUri.scheme == "content") {
			try {
				streamReader = InputStreamReader(contentResolver.openInputStream(dataUri))
			} catch (e: FileNotFoundException) {
				e.printStackTrace()
			}
		} else {
			val juri: URI
			Log.v(javaClass.simpleName, "$dataUri")
			try {
				juri = URI(
					dataUri.scheme, dataUri.schemeSpecificPart, dataUri.fragment
				)
				streamReader = InputStreamReader(juri.toURL().openStream())
			} catch (e: URISyntaxException) {
				e.printStackTrace()
			} catch (e: IOException) {
				e.printStackTrace()
			}
		}

		if (streamReader == null) {
			return
		}

		val cBuffer = CharArray(512)
		val numberOfCharactersRead: Int
		try {
			// read first 512 bytes to check the type of file
			numberOfCharactersRead = streamReader.read(cBuffer, 0, 512)
			streamReader.close()
		} catch (e: IOException) {
			return
		}

		if (numberOfCharactersRead < 81) {
			// At least one full 9x9 game needed in case of SDM
			return
		}

		val cBufferStr = String(cBuffer)

		@Suppress("RegExpSimplifiable")
		val importTask: AbstractImportTask = if (cBufferStr.contains("<opensudoku")) { // Recognized OpenSudoku file
			OpenSudokuImportTask(dataUri)
		} else if (cBufferStr.matches("""[.0-9\n\r]{$numberOfCharactersRead}""".toRegex())) { // Recognized Sudoku SDM file
			SdmImportTask(dataUri)
		} else if (SepbRegex.containsMatchIn(cBufferStr)) { // Recognized Sudoku Exchange "Puzzle Bank" file
			SepbImportTask(dataUri)
		} else {
			Log.e(javaClass.simpleName, "Unknown type of data provided (mime-type=${intent.type}; uri=$dataUri), exiting.")
			Toast.makeText(this, R.string.invalid_format, Toast.LENGTH_LONG).show()
			finish()
			return
		}

		with(ProgressUpdater(applicationContext, findViewById(R.id.progress_bar))) {
			titleTextView = findViewById(R.id.progress_introduction)
			titleStringRes = R.string.importing_from
			progressTextView = findViewById(R.id.progress_status_text)
			progressStringRes = R.string.importing_status
			progressStringResMaxOnly = R.string.import_puzzles_found
			progressStringResNoValues = R.string.checking_duplicates
			this
		}.use { progressUpdater ->
			CoroutineScope(Dispatchers.IO).launch {
				importTask.importFromFile(applicationContext, ::onImportFinishedListener, supportFragmentManager, progressUpdater)
			}
		}
	}

	/**
	 * Occurs when import is finished.
	 *
	 * @param importSuccessful Indicates whether import was successful.
	 * @param folderId         Contains id of imported folder, or -1 if multiple folders were imported.
	 */
	private fun onImportFinishedListener(importSuccessful: Boolean, folderId: Long) {
		if (importSuccessful) {
			if (folderId != -1L) {
				// only one folder was imported, open this folder automatically
				val i = Intent(this@PuzzleImportActivity, PuzzleListActivity::class.java)
				i.putExtra(Tag.FOLDER_ID, folderId)
				startActivity(i)
			}
		}
		finish()
	}
}
