/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2024 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.gui

import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import androidx.appcompat.app.AlertDialog
import androidx.preference.PreferenceManager
import org.moire.opensudoku.R
import java.util.LinkedList
import java.util.Queue

class HintsQueue(private val mContext: Context) {
	private val mMessages: Queue<Message>
	private val mHintDialog: AlertDialog
	private val mPrefs: SharedPreferences = mContext.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
	private var mOneTimeHintsEnabled: Boolean

	init {
		val gameSettings = PreferenceManager.getDefaultSharedPreferences(mContext)
		gameSettings.registerOnSharedPreferenceChangeListener { sharedPreferences: SharedPreferences, key: String? ->
			if (key == "show_hints") {
				mOneTimeHintsEnabled = sharedPreferences.getBoolean("show_hints", true)
			}
		}
		mOneTimeHintsEnabled = gameSettings.getBoolean("show_hints", true)

		//processQueue();
		val mHintClosed = DialogInterface.OnClickListener { _: DialogInterface?, _: Int -> }
		mHintDialog = AlertDialog.Builder(mContext)
			.setIcon(R.drawable.ic_info)
			.setTitle(R.string.hint)
			.setMessage("")
			.setPositiveButton(R.string.close, mHintClosed)
			.create()
		mHintDialog.setOnDismissListener { processQueue() }
		mMessages = LinkedList()
	}

	private fun addHint(hint: Message) {
		synchronized(mMessages) { mMessages.add(hint) }
		synchronized(mHintDialog) {
			if (!mHintDialog.isShowing) {
				processQueue()
			}
		}
	}

	private fun processQueue() {
		showHintDialog(synchronized(mMessages) { mMessages.poll() ?: return@processQueue })
	}

	private fun showHintDialog(hint: Message) {
		synchronized(mHintDialog) {
			mHintDialog.setTitle(mContext.getString(hint.titleResID))
			mHintDialog.setMessage(mContext.getText(hint.messageResID))
			mHintDialog.show()
		}
	}

	fun showHint(titleResID: Int, messageResID: Int) {
		val hint = Message()
		hint.titleResID = titleResID
		hint.messageResID = messageResID
		//hint.args = args;
		addHint(hint)
	}

	fun showOneTimeHint(key: String, titleResID: Int, messageResID: Int) {
		if (mOneTimeHintsEnabled) {

			val hintKey = "hint_$key"
			if (!mPrefs.getBoolean(hintKey, false)) {
				showHint(titleResID, messageResID)
				val editor = mPrefs.edit()
				editor.putBoolean(hintKey, true)
				editor.apply()
			}
		}
	}

	private class Message {
		var titleResID = 0
		var messageResID = 0 //Object[] args;
	}

	companion object {
		private const val PREF_FILE_NAME = "hints"
	}
}
