/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2024 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteQueryBuilder
import android.util.Log
import org.moire.opensudoku.game.CellCollection
import org.moire.opensudoku.game.FolderInfo
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.gui.PuzzleListFilter
import java.io.Closeable
import java.time.Instant
import java.util.LinkedList

/**
 * Wrapper around opensudoku's database.
 * You have to close connection when you're done with the database.
 */
class SudokuDatabase(context: Context, readOnly: Boolean) : Closeable {
	private val mOpenHelper: DatabaseHelper = DatabaseHelper(context)
	private val db: SQLiteDatabase = if (readOnly) mOpenHelper.readableDatabase else mOpenHelper.writableDatabase

	// FOLDER METHODS

	/**
	 * Returns list of puzzle folders.
	 */
	fun getFolderList(withCounts: Boolean = false): List<FolderInfo> {
		val qb = SQLiteQueryBuilder()
		val folderList: MutableList<FolderInfo> = LinkedList()
		qb.tables = FOLDERS_TABLE_NAME
		qb.query(db, null, null, null, null, null, null).forEach { cursor ->
			val folderInfo = FolderInfo()
			folderInfo.id = cursor.id
			folderInfo.name = cursor.name
			folderInfo.created = cursor.getLong(cursor.getColumnIndexOrThrow(Column.CREATED))
			if (withCounts) {
				folderList.add(getFolderInfoWithCounts(folderInfo.id))
			} else {
				folderList.add(folderInfo)
			}
		}
		return folderList
	}

	/**
	 * Returns the folder info.
	 *
	 * @param folderId Primary key of folder.
	 */
	fun getFolderInfo(folderId: Long): FolderInfo? {
		val qb = SQLiteQueryBuilder()
		qb.tables = FOLDERS_TABLE_NAME
		qb.query(db, null, Column.ID + "=" + folderId, null, null, null, null)
			.use { cursor ->
				return@getFolderInfo if (cursor.moveToFirst()) {
					val id = cursor.id
					val name = cursor.name
					val folderInfo = FolderInfo()
					folderInfo.id = id
					folderInfo.name = name
					folderInfo
				} else {
					null
				}
			}
	}

	/**
	 * Returns the folder info.
	 *
	 * @param folderName Name of the folder to get info.
	 */
	private fun getFolderInfo(folderName: String): FolderInfo? {
		val qb = SQLiteQueryBuilder()
		qb.tables = FOLDERS_TABLE_NAME
		qb.query(db, null, Column.NAME + "=?", arrayOf(folderName), null, null, null).use { cursor ->
			return@getFolderInfo if (cursor.moveToFirst()) FolderInfo(cursor.id, cursor.name) else null
		}
	}

	/**
	 * Returns the full folder info - this includes count of games in particular states.
	 *
	 * @param folderId Primary key of folder.
	 * @return folder info
	 */
	fun getFolderInfoWithCounts(folderId: Long): FolderInfo {
		val puzzleCountColumn = "puzzle_count"
		val folder = FolderInfo(folderId, "")
		val q = """
SELECT
	f.${Column.ID} AS ${Column.ID},
	f.${Column.NAME} AS ${Column.NAME},
	p.${Column.STATE} AS ${Column.STATE},
	COUNT(*) AS $puzzleCountColumn
FROM $FOLDERS_TABLE_NAME f INNER JOIN $PUZZLES_TABLE_NAME p ON f.${Column.ID} = p.${Column.FOLDER_ID}
WHERE f.${Column.ID} = $folderId
GROUP BY p.${Column.STATE}
"""
		db.rawQuery(q, null).use { cursor ->
			if (cursor.count < 1) return@getFolderInfoWithCounts getFolderInfo(folderId) ?: folder
			while (cursor.moveToNext()) {
				val state = cursor.getInt(cursor.getColumnIndexOrThrow(Column.STATE))
				val count = cursor.getInt(cursor.getColumnIndexOrThrow(puzzleCountColumn))
				if (folder.name.isBlank()) {
					folder.name = cursor.name
				}
				folder.puzzleCount += count
				if (state == SudokuGame.GAME_STATE_COMPLETED) {
					folder.solvedCount += count
				} else if (state == SudokuGame.GAME_STATE_PLAYING) {
					folder.playingCount += count
				}
			}
		}
		return folder
	}

	/**
	 * Inserts new puzzle folder into the database.
	 *
	 * @param name    Name of the folder.
	 * @param created Time of folder creation.
	 */
	fun insertFolder(name: String, created: Long): FolderInfo {
		val existingFolder = getFolderInfo(name)
		if (existingFolder != null) {
			return existingFolder
		}
		val values = ContentValues()
		values.put(Column.CREATED, created)
		values.put(Column.NAME, name)
		val rowId = db.insert(FOLDERS_TABLE_NAME, Column.ID, values)
		if (rowId < 0) {
			throw SQLException("Failed to insert folder '$name'.")
		}
		return FolderInfo(rowId, name)
	}

	/**
	 * Renames existing folder.
	 *
	 * @param folderId Primary key of folder.
	 * @param name     New name for the folder.
	 */
	fun renameFolder(folderId: Long, name: String) {
		val values = ContentValues()
		values.put(Column.NAME, name)
		db.update(FOLDERS_TABLE_NAME, values, Column.ID + "=" + folderId, null)
	}

	/**
	 * Deletes given folder.
	 *
	 * @param folderId Primary key of folder.
	 */
	fun deleteFolder(folderId: Long) {
		// delete all puzzles in folder we are going to delete
		db.delete(PUZZLES_TABLE_NAME, Column.FOLDER_ID + "=" + folderId, null)
		// delete the folder
		db.delete(FOLDERS_TABLE_NAME, Column.ID + "=" + folderId, null)
	}

	// PUZZLE METHODS

	/**
	 * Deletes given puzzle from the database.
	 */
	fun deletePuzzle(puzzleID: Long) {
		db.delete(PUZZLES_TABLE_NAME, Column.ID + "=" + puzzleID, null)
	}

	fun findPuzzle(cells: CellCollection): SudokuGame? {
		with(SQLiteQueryBuilder()) {
			tables = PUZZLES_TABLE_NAME
			query(
				db, null, Column.ORIGINAL_VALUES + "=?", arrayOf(cells.originalValues), null, null, null
			).use { cursor ->
				if (cursor.moveToFirst()) return@findPuzzle extractSudokuGameFromCursorRow(cursor)
			}
		}
		return null
	}

	/**
	 * Returns sudoku game object.
	 *
	 * @param gameID Primary key of folder.
	 */
	internal fun getPuzzle(gameID: Long): SudokuGame? {
		with(SQLiteQueryBuilder()) {
			tables = PUZZLES_TABLE_NAME
			query(db, null, Column.ID + "=" + gameID, null, null, null, null).use { cursor ->
				if (cursor.moveToFirst()) {
					return@getPuzzle extractSudokuGameFromCursorRow(cursor)
				}
			}
		}
		return null
	}

	internal fun insertPuzzle(originalValues: String, folderId: Long) {
		with(ContentValues()) {
			put(Column.ORIGINAL_VALUES, originalValues)
			put(Column.CREATED, Instant.now().toEpochMilli())
			put(Column.LAST_PLAYED, 0)
			put(Column.STATE, SudokuGame.GAME_STATE_NOT_STARTED)
			put(Column.MISTAKE_COUNTER, 0)
			put(Column.TIME, 0)
			put(Column.USER_NOTE, "")
			put(Column.COMMAND_STACK, "")
			put(Column.FOLDER_ID, folderId)
			val rowId = db.insert(PUZZLES_TABLE_NAME, null, this)
			if (rowId < 0) {
				throw SQLException("Failed to insert puzzle.")
			}
		}
	}

	internal fun insertPuzzle(newGame: SudokuGame): Long {
		val rowId = db.insert(PUZZLES_TABLE_NAME, null, newGame.contentValues)
		if (rowId < 0) {
			throw SQLException("Failed to insert puzzle.")
		}
		return rowId
	}

	internal fun updatePuzzle(game: SudokuGame): Int = db.update(PUZZLES_TABLE_NAME, game.contentValues, "${Column.ID}=${game.id}", null)

	internal fun resetAllPuzzles(folderID: Long) {
		with(ContentValues()) {
			putNull(Column.CELLS_DATA)
			put(Column.LAST_PLAYED, 0)
			put(Column.STATE, SudokuGame.GAME_STATE_NOT_STARTED)
			put(Column.MISTAKE_COUNTER, 0)
			put(Column.TIME, 0)
			put(Column.USER_NOTE, "")
			put(Column.COMMAND_STACK, "")
			val rowsCount = db.update(PUZZLES_TABLE_NAME, this, "${Column.FOLDER_ID}=$folderID", null)
			if (rowsCount <= 0) {
				throw SQLException("Failed to update puzzles.")
			}
		}
	}

	/**
	 * Returns list of sudoku game objects
	 *
	 * @param folderId Primary key of folder.
	 */
	internal fun getPuzzleListCursor(folderId: Long = 0L, filter: PuzzleListFilter? = null, sortOrder: String? = null): Cursor {
		val qb = SQLiteQueryBuilder()
		qb.tables = PUZZLES_TABLE_NAME
		if (folderId != 0L) {
			qb.appendWhere(Column.FOLDER_ID + "=" + folderId)
		}
		if (filter != null) {
			if (!filter.showStateCompleted) {
				qb.appendWhere(" and " + Column.STATE + "!=" + SudokuGame.GAME_STATE_COMPLETED)
			}
			if (!filter.showStateNotStarted) {
				qb.appendWhere(" and " + Column.STATE + "!=" + SudokuGame.GAME_STATE_NOT_STARTED)
			}
			if (!filter.showStatePlaying) {
				qb.appendWhere(" and " + Column.STATE + "!=" + SudokuGame.GAME_STATE_PLAYING)
			}
		}
		return qb.query(db, null, null, null, null, null, sortOrder)
	}

	fun folderExists(folderName: String): Boolean = getFolderInfo(folderName) != null

	override fun close() {
		mOpenHelper.close()
	}
}

internal fun extractSudokuGameFromCursorRow(cursor: Cursor): SudokuGame {
	val game: SudokuGame
	try {
		val cellsDataIndex = cursor.getColumnIndexOrThrow(Column.CELLS_DATA)
		val (cells, dataVersion) = CellCollection.deserialize(
			cursor.getString(
				if (!cursor.isNull(cellsDataIndex)) cellsDataIndex else cursor.getColumnIndexOrThrow(Column.ORIGINAL_VALUES),
			),
		)
		game = SudokuGame(cells)
		with(game) {
			id = cursor.id
			created = cursor.getLong(cursor.getColumnIndexOrThrow(Column.CREATED))
			lastPlayed = cursor.getLong(cursor.getColumnIndexOrThrow(Column.LAST_PLAYED))
			state = cursor.getInt(cursor.getColumnIndexOrThrow(Column.STATE))
			mistakeCounter = cursor.getInt(cursor.getColumnIndexOrThrow(Column.MISTAKE_COUNTER))
			playingDuration = cursor.getLong(cursor.getColumnIndexOrThrow(Column.TIME))
			userNote = cursor.getString(cursor.getColumnIndexOrThrow(Column.USER_NOTE)) ?: ""
			folderId = cursor.getLong(cursor.getColumnIndexOrThrow(Column.FOLDER_ID))
			commandStack.deserialize(cursor.getString(cursor.getColumnIndexOrThrow(Column.COMMAND_STACK)), dataVersion)
		}
	} catch (e: Exception) {    // this shouldn't normally happen, db corrupted
		Log.e(DB_TAG, "Error extracting SudokuGame from cursor", e)
		return SudokuGame.createEmptyGame()
	}
	return game
}

internal fun Cursor.forEach(callback: ((Cursor) -> Unit)) {
	if (moveToFirst()) {
		while (!isAfterLast) {
			callback(this)
			moveToNext()
		}
	}
	close()
}

internal val Cursor.originalValues: String
	get() = getString(getColumnIndexOrThrow(Column.ORIGINAL_VALUES))

internal val Cursor.id: Long
	get() = getLong(getColumnIndexOrThrow(Column.ID))

internal val Cursor.name: String
	get() = getString(getColumnIndexOrThrow(Column.NAME)) ?: ""

const val DB_TAG = "SudokuDatabase"
