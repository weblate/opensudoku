/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.game

import android.content.ContentValues
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import org.moire.opensudoku.db.Column
import org.moire.opensudoku.game.CellCollection.Companion.SUDOKU_SIZE
import org.moire.opensudoku.game.command.AbstractCommand
import org.moire.opensudoku.game.command.ClearAllMarksCommand
import org.moire.opensudoku.game.command.CommandStack
import org.moire.opensudoku.game.command.EditCellCentralMarksCommand
import org.moire.opensudoku.game.command.EditCellCornerMarksCommand
import org.moire.opensudoku.game.command.FillInMarksCommand
import org.moire.opensudoku.game.command.FillInMarksWithAllValuesCommand
import org.moire.opensudoku.game.command.SetCellValueAndRemoveMarksCommand
import org.moire.opensudoku.game.command.SetCellValueCommand
import org.moire.opensudoku.gui.Tag
import java.time.Instant

class SudokuGame(initialCells: CellCollection) {
	var id: Long = -1
	var folderId: Long = -1
	var created: Long = 0
	var state: Int
	var mistakeCounter: Int = 0
	var lastPlayed: Long = 0
	var userNote: String = ""
	private var mUsedSolver = false
	internal var removeMarksOnEntry = true
	internal var checkIndirectErrors: Boolean = true
	internal var onPuzzleSolvedListener: (() -> Unit)? = null
	internal var onDigitFinishedManuallyListener: ((Int) -> Unit)? = null
	internal var onInvalidDigitEnteredListener: (() -> Unit)? = null
	private var mActiveFromTime: Long = -1 // time when current activity has become active.

	var mCells: CellCollection = initialCells
		internal set(newCells) {
			field = newCells
			newCells.validate()
			commandStack = CommandStack(newCells)
		}
	var onHasUndoChangedListener: (() -> Unit)? = null
		set(newValue) {
			field = newValue
			commandStack.onEmptyChangeListener = newValue
		}
	var commandStack: CommandStack = CommandStack(mCells)
		set(value) {
			field = value
			field.onEmptyChangeListener = onHasUndoChangedListener
		}

	/**
	 * Time of game-play in milliseconds.
	 */
	var playingDuration: Long = 0
		get() = (if (mActiveFromTime != -1L) field + SystemClock.uptimeMillis() - mActiveFromTime else field)
		set(value) {
			field = value
			if (mActiveFromTime != -1L) {
				mActiveFromTime = SystemClock.uptimeMillis()
			}
		}

	val contentValues: ContentValues
		get() {
			with(ContentValues()) {
				put(Column.ORIGINAL_VALUES, mCells.originalValues)
				put(Column.CELLS_DATA, mCells.serialize())
				put(Column.CREATED, created)
				put(Column.LAST_PLAYED, lastPlayed)
				put(Column.STATE, state)
				put(Column.MISTAKE_COUNTER, mistakeCounter)
				put(Column.TIME, playingDuration)
				put(Column.USER_NOTE, userNote)
				put(Column.COMMAND_STACK, commandStack.serialize())
				put(Column.FOLDER_ID, folderId)
				return this
			}
		}

	init {
		state = GAME_STATE_NOT_STARTED
	}

	fun saveState(outState: Bundle) {
		with(outState) {
			putLong(Tag.PUZZLE_ID, id)
			putLong(Tag.CREATED, created)
			putInt(Tag.STATE, state)
			putInt(Tag.MISTAKE_COUNTER, mistakeCounter)
			putLong(Tag.TIME, playingDuration)
			putLong(Tag.LAST_PLAYED, lastPlayed)
			putString(Tag.CELLS_DATA, mCells.serialize())
			putString(Tag.USER_NOTE, userNote)
			putString(Tag.COMMAND_STACK, commandStack.serialize())
			putLong(Tag.FOLDER_ID, folderId)
		}
	}

	fun restoreState(inState: Bundle) {
		try {
			id = inState.getLong(Tag.PUZZLE_ID)
			created = inState.getLong(Tag.CREATED)
			state = inState.getInt(Tag.STATE)
			mistakeCounter = inState.getInt(Tag.MISTAKE_COUNTER)
			playingDuration = inState.getLong(Tag.TIME)
			lastPlayed = inState.getLong(Tag.LAST_PLAYED)
			val (cells, dataVersion) = CellCollection.deserialize(inState.getString(Tag.CELLS_DATA) ?: "")
			mCells = cells
			userNote = inState.getString(Tag.USER_NOTE) ?: ""
			commandStack.deserialize(inState.getString(Tag.COMMAND_STACK), dataVersion)
			folderId = inState.getLong(Tag.FOLDER_ID)
			mCells.validate()
		} catch (e: Exception) {    // this shouldn't normally happen, stored state corrupted
			Log.e(javaClass.simpleName, "Error restoring Game state", e)
		}
	}

	/**
	 * Sets value for the given cell. 0 means empty cell.
	 */
	fun setCellValue(cell: Cell, value: Int, isManual: Boolean) {
		if (!cell.isEditable || cell.value == value) return
		require(!(value < 0 || value > 9)) { "Value must be between 0-9." }

		if (removeMarksOnEntry) {
			executeCommand(SetCellValueAndRemoveMarksCommand(cell, value), isManual)
		} else {
			executeCommand(SetCellValueCommand(cell, value), isManual)
		}

		if (isSolved) {
			markGameAsCompletedAndPause()
			onPuzzleSolvedListener?.invoke()
		}

		if (isManual && value > 0) {
			val isValid = if (checkIndirectErrors) cell.matchesSolution else mCells.validate()
			if (!isValid) {
				onInvalidDigitEnteredListener?.invoke()
				mistakeCounter += 1
			} else if (mCells.valuesUseCount[value] == 9 && !mCells.hasMistakes) {
				onDigitFinishedManuallyListener?.invoke(value)
			}
		}
	}

	/**
	 * Sets central marks attached to the given cell.
	 */
	fun setCellCentralMarks(cell: Cell, marks: CellMarks, isManual: Boolean): Boolean {
		if (cell.isEditable && cell.centralMarks != marks) {
			executeCommand(EditCellCentralMarksCommand(cell, marks), isManual)
			return true
		}
		return false
	}

	/**
	 * Sets corner marks attached to the given cell.
	 */
	fun setCellCornerMarks(cell: Cell, marks: CellMarks, isManual: Boolean): Boolean {
		if (cell.isEditable && cell.cornerMarks != marks) {
			executeCommand(EditCellCornerMarksCommand(cell, marks), isManual)
			return true
		}
		return false
	}

	private fun executeCommand(c: AbstractCommand, isManual: Boolean) {
		commandStack.execute(c, isManual)
	}

	/**
	 * Undo last command.
	 */
	fun undo(): Cell? = commandStack.undo()

	fun hasSomethingToUndo(): Boolean = !commandStack.isEmpty

	fun setUndoCheckpoint() {
		commandStack.setCheckpoint()
	}

	fun undoToCheckpoint() {
		commandStack.undoToCheckpoint()
	}

	fun hasUndoCheckpoint(): Boolean = commandStack.hasCheckpoint()

	fun undoToBeforeMistake() {
		commandStack.undoToSolvableState()
	}

	val lastCommandCell: Cell?
		get() = commandStack.lastCommandCell

	/**
	 * Start game-play.
	 */
	fun start() {
		state = GAME_STATE_PLAYING
		resume()
	}

	fun resume() {
		// reset time we have spent playing so far, so time when activity was not active
		// will not be part of the game play time
		mActiveFromTime = SystemClock.uptimeMillis()
	}

	/**
	 * Pauses game-play (for example if activity pauses).
	 */
	fun pause() {
		// save time we have spent playing so far - it will be reset after resuming
		val lastPlayingDuration = playingDuration // getter calculated value
		mActiveFromTime = -1L
		playingDuration = lastPlayingDuration
		lastPlayed = Instant.now().toEpochMilli()
	}

	val solutionCount: Int
		get() = mCells.solutionCount

	/**
	 * Solves puzzle from original state
	 */
	fun solve(): Int {
		mUsedSolver = true
		if (mCells.solutionCount != 1) {
			return mCells.solutionCount
		}
		for (row in 0..<SUDOKU_SIZE) {
			for (col in 0..<SUDOKU_SIZE) {
				val cell = mCells.getCell(row, col)
				setCellValue(cell, cell.solution, false)
			}
		}
		commandStack.clean()
		return 1
	}

	fun usedSolver(): Boolean = mUsedSolver

	/**
	 * Solves puzzle and fills in correct value for selected cell
	 */
	fun solveCell(cell: Cell) {
		require(mCells.solutionCount == 1) { "This puzzle has " + mCells.solutionCount + " solutions" }
		setCellValue(cell, cell.solution, true)
	}

	/**
	 * Pauses game-play and sets state to completed. Called when puzzle is solved.
	 */
	private fun markGameAsCompletedAndPause() {
		pause()
		state = GAME_STATE_COMPLETED
	}

	/**
	 * Resets game.
	 */
	fun reset() {
		for (r in 0..<SUDOKU_SIZE) {
			for (c in 0..<SUDOKU_SIZE) {
				val cell = mCells.getCell(r, c)
				if (cell.isEditable) {
					cell.value = 0
				}
				cell.centralMarks = CellMarks()
				cell.cornerMarks = CellMarks()
			}
		}
		commandStack = CommandStack(mCells)
		mCells.validate()
		mistakeCounter = 0
		playingDuration = 0
		lastPlayed = 0
		state = GAME_STATE_NOT_STARTED
		mUsedSolver = false
	}

	/**
	 * Returns true, if puzzle is solved. In order to know the current state, you have to call validate first.
	 */
	private val isSolved: Boolean
		get() = mCells.isSolved

	fun clearAllMarksManual() = executeCommand(ClearAllMarksCommand(), true)

	/**
	 * Fills in possible values which can be entered in each cell.
	 */
	fun fillInMarksManual() = executeCommand(FillInMarksCommand(), true)

	/**
	 * Fills in all values which can be entered in each cell.
	 */
	fun fillInMarksWithAllValuesManual() = executeCommand(FillInMarksWithAllValuesCommand(), true)

	companion object {
		const val GAME_STATE_PLAYING = 0
		const val GAME_STATE_NOT_STARTED = 1
		const val GAME_STATE_COMPLETED = 2
		fun createEmptyGame(): SudokuGame {
			val game = SudokuGame(CellCollection.createEmpty())
			game.created = Instant.now().toEpochMilli() // set creation time
			return game
		}
	}
}
