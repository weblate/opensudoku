/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.command

import org.moire.opensudoku.game.CellCollection
import org.moire.opensudoku.game.CellMarks

class ClearAllMarksCommand : AbstractMultiMarksCommand() {
	override fun execute() {
		val cells = cells
		mCentralMarksBeforeCommandEntries.clear()
		mCornerMarksBeforeCommandEntries.clear()
		for (r in 0..<CellCollection.SUDOKU_SIZE) {
			for (c in 0..<CellCollection.SUDOKU_SIZE) {
				val cell = cells.getCell(r, c)
				if (!cell.centralMarks.isEmpty) {
					mCentralMarksBeforeCommandEntries.add(MarksEntry(r, c, cell.centralMarks))
					cell.centralMarks = CellMarks()
				}
				if (!cell.cornerMarks.isEmpty) {
					mCornerMarksBeforeCommandEntries.add(MarksEntry(r, c, cell.cornerMarks))
					cell.cornerMarks = CellMarks()
				}
			}
		}
	}
}
