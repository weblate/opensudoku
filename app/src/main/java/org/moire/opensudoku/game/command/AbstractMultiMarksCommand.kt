/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.command

import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.CellCollection
import org.moire.opensudoku.game.CellMarks
import java.util.StringTokenizer

abstract class AbstractMultiMarksCommand : AbstractCellCommand() {
	protected var mCentralMarksBeforeCommandEntries: MutableList<MarksEntry> = ArrayList()
	protected var mCornerMarksBeforeCommandEntries: MutableList<MarksEntry> = ArrayList()
	override fun serialize(data: StringBuilder) {
		super.serialize(data)
		data.append(mCentralMarksBeforeCommandEntries.size).append("|")
		for (marksEntry in mCentralMarksBeforeCommandEntries) {
			data.append(marksEntry.rowIndex).append("|")
			data.append(marksEntry.colIndex).append("|")
			marksEntry.marks.serialize(data)
		}
		data.append(mCornerMarksBeforeCommandEntries.size).append("|")
		for (marksEntry in mCornerMarksBeforeCommandEntries) {
			data.append(marksEntry.rowIndex).append("|")
			data.append(marksEntry.colIndex).append("|")
			marksEntry.marks.serialize(data)
		}
	}

	override fun deserialize(data: StringTokenizer, dataVersion: Int) {
		super.deserialize(data, dataVersion)
		var marksSize = data.nextToken().toInt()
		for (i in 0..<marksSize) {
			val row = data.nextToken().toInt()
			val col = data.nextToken().toInt()
			mCentralMarksBeforeCommandEntries.add(MarksEntry(row, col, CellMarks.deserialize(data.nextToken())))
		}

		// There are no tokens for secondary marks if deserializing data from before double marks existed.
		if (dataVersion < CellCollection.DATA_VERSION_4 || !data.hasMoreTokens()) {
			return
		}
		marksSize = data.nextToken().toInt()
		for (i in 0..<marksSize) {
			val row = data.nextToken().toInt()
			val col = data.nextToken().toInt()
			mCornerMarksBeforeCommandEntries.add(MarksEntry(row, col, CellMarks.deserialize(data.nextToken())))
		}
	}

	override fun undo(): Cell? {
		for (marksEntry in mCentralMarksBeforeCommandEntries) {
			cells.getCell(marksEntry.rowIndex, marksEntry.colIndex).centralMarks = marksEntry.marks
		}
		for (marksEntry in mCornerMarksBeforeCommandEntries) {
			cells.getCell(marksEntry.rowIndex, marksEntry.colIndex).cornerMarks = marksEntry.marks
		}
		return null
	}

	protected fun saveOldMarks() {
		for (row in 0..<CellCollection.SUDOKU_SIZE) {
			for (col in 0..<CellCollection.SUDOKU_SIZE) {
				mCentralMarksBeforeCommandEntries.add(MarksEntry(row, col, cells.getCell(row, col).centralMarks))
				mCornerMarksBeforeCommandEntries.add(MarksEntry(row, col, cells.getCell(row, col).cornerMarks))
			}
		}
	}

	protected class MarksEntry(var rowIndex: Int, var colIndex: Int, var marks: CellMarks)
}
